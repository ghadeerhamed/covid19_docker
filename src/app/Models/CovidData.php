<?php

namespace App\Models;

use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CovidData
 * @package App\Models
 *
 * @property integer $id
 * @property string $date
 * @property string $confirmed_injuries
 * @property-read Country | Model country
 */
class CovidData extends Model
{
    protected $guarded = ['id'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
